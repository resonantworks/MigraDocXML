<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>
    
    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.1 - Conditionals</p>

        <p>All of the XML we've written up to this point has been working with Display elements, that is, the element you create produces some tangible output on the generated PDF. In this lesson we start to cover the first of the Logical elements, these don't add any content to your generated document, though that doesn't make them any less useful.</p>

        <p>For example, conditionals allow you to make decisions based on the outcome of some criteria. As a simple example:</p>

        <p Style="Code">
            &lt;If Test=&quot;```{0 = 1}```&quot;&gt;
            {Space(4)}&lt;p&gt;Maths is broken!&lt;/p&gt;
            &lt;/If&gt;
            &lt;Else&gt;
            {Space(4)}&lt;p&gt;Everything is normal.&lt;/p&gt;
            &lt;/Else&gt;
            
        </p>

        <p>Produces:</p>

        <If Test="{0 == 1}">
            <p>Maths is broken!</p>
        </If>
        <Else>
            <p>Everything is normal.</p>
        </Else>

        <p>The Test attribute of <b>If</b> accepts a script which gets evaluated. If the result is true, then any contents inside of the <b>If</b> get run. The <b>Else</b> element afterwards is optional, this only gets run if the result of the <b>If</b>'s Test was false.</p>

        <p>You can also optionally include any number of <b>ElseIf</b>s after an <b>If</b>. These also run a test, where if the Test returns true, the contents are run, otherwise execution moves down to the next <b>ElseIf</b>/<b>Else</b>.</p>

        <p Style="Code">
            &lt;If Test=&quot;1 + 2 = 4&quot;&gt;
            {Space(4)}&lt;p&gt;Apple&lt;/p&gt;
            &lt;/If&gt;
            &lt;ElseIf Test=&quot;3 * 5 = 14&quot;&gt;
            {Space(4)}&lt;p&gt;Banana&lt;/p&gt;
            &lt;/ElseIf&gt;
            &lt;ElseIf Test=&quot;4 - 2 = 2&quot;&gt;
            {Space(4)}&lt;p&gt;Cat&lt;/p&gt;
            &lt;/ElseIf&gt;
            &lt;Else&gt;
            {Space(4)}&lt;p&gt;Dog&lt;/p&gt;
            &lt;/Else&gt;

        </p>

        <p>Produces:</p>

        <If Test="1 + 2 = 4">
            <p>Apple</p>
        </If>
        <ElseIf Test="3 * 5 = 14">
            <p>Banana</p>
        </ElseIf>
        <ElseIf Test="4 - 2 = 2">
            <p>Cat</p>
        </ElseIf>
        <Else>
            <p>Dog</p>
        </Else>

        <p>If you're particularly observant, you may have noticed that in the above example, none of the Test attributes included the curly braces. The curly braces are actually optional here, since the Test attribute cannot contain anything other than a script, so there is no need to explicitly signify it as such. This is true for all attributes of Logical elements that only accept script contents.</p>

    </Section>
</Document>