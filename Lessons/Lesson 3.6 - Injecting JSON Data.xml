<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>
    
    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.6 - Injecting JSON Data</p>

        <p>CSV is fine if you've got very simple data to work with, but for more complex data structures, JSON is a far better option. In this lesson we look at generating a very simple order confirmation document for a customer. It's advised you also look at the accompanying JSON file for this lesson.</p>

        <p>As with CSVs, we first of all need to tell MigraDocXML where to find our JSON data. On the PdfXmlReader object, just call <b>RunWithJsonFile(path to file)</b> instead of <b>Run()</b>.</p>

        <p>The example below is a bit of a step up in complexity, but if you've been through all of the previous lessons, you should be able to follow what is being done.</p>

        <p Style="Code">
            &lt;p Format.Font.Size=&quot;16&quot; Format.Font.Underline=&quot;Single&quot;
            {Space(3)}Format.Font.Bold=&quot;true&quot; Format.Alignment=&quot;Center&quot;&gt;Order Confirmation: ```{Model.OrderNumber}```&lt;/p&gt;
            &lt;p&gt;Customer: ```{Model.Customer.Name}```&lt;/p&gt;
            &lt;p&gt;Account: ```{Model.Customer.AccountNumber}```&lt;/p&gt;
            &lt;p&gt;Date Raised: ```{Model.Date #dd MMM yyyy}```&lt;/p&gt;

            &lt;Table Borders.Color=&quot;Black&quot;&gt;
            {Space(4)}&lt;Column Width=&quot;6cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;3cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;3cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;3cm&quot;/&gt;

            {Space(4)}&lt;Row Heading=&quot;true&quot; Format.Font.Bold=&quot;true&quot; 
            {Space(9)}C0=&quot;Product&quot; C1=&quot;Quantity&quot; C2=&quot;Unit Price&quot; C3=&quot;Line Price&quot;/&gt;
            
            {Space(4)}&lt;Var totalPrice=&quot;0&quot;/&gt;
            {Space(4)}&lt;ForEach Var=&quot;orderLine&quot; In=&quot;Model.OrderLines&quot;&gt;
            {Space(8)}&lt;Var qty=&quot;orderLine.Quantity&quot; unitPrice=&quot;orderLine.Product.Price&quot;/&gt;
            {Space(8)}&lt;Var linePrice=&quot;qty * unitPrice&quot;/&gt;
            {Space(8)}&lt;Set totalPrice=&quot;totalPrice + linePrice&quot;/&gt;

            {Space(8)}&lt;Row C0=&quot;```{orderLine.Product.Name}```&quot; C1=&quot;```{qty}```&quot; C2=&quot;```{unitPrice #C}```&quot; C3=&quot;```{linePrice #C}```&quot;/&gt;
            {Space(4)}&lt;/ForEach&gt;
            
            {Space(4)}&lt;Row Format.Font.Bold=&quot;true&quot; C0=&quot;Total&quot; C3=&quot;```{totalPrice #C}```&quot;/&gt;
            &lt;/Table&gt;
        </p>

        <p>Produces:</p>

        <p Format.Font.Size="16" Format.Font.Underline="Single" 
           Format.Font.Bold="true" Format.Alignment="Center">Order Confirmation: {Model.OrderNumber}</p>
        <p>Customer: {Model.Customer.Name}</p>
        <p>Account: {Model.Customer.AccountNumber}</p>
        <p>Date Raised: {Model.Date #dd MMM yyyy}</p>

        <Table Borders.Color="Black">
            <Column Width="6cm"/>
            <Column Width="3cm"/>
            <Column Width="3cm"/>
            <Column Width="3cm"/>

            <Row Heading="true" Format.Font.Bold="true" C0="Product" C1="Quantity" C2="Unit Price" C3="Line Price"/>
            <Var totalPrice="0"/>
            <ForEach Var="orderLine" In="Model.OrderLines">
                <Var qty="orderLine.Quantity" unitPrice="orderLine.Product.Price"/>
                <Var linePrice="qty * unitPrice"/>
                <Row C0="{orderLine.Product.Name}" C1="{qty}" C2="{unitPrice #C}" C3="{linePrice #C}"/>
                <Set totalPrice="totalPrice + linePrice"/>
            </ForEach>
            <Row Format.Font.Bold="true" C0="Total" C3="{totalPrice #C}"/>
        </Table>
        
    </Section>
</Document>