<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

  <Style Target="Paragraph">
    <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
  </Style>

  <Style Target="Paragraph" Name="Title">
    <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
  </Style>

  <Style Target="Paragraph" Name="SubTitle">
    <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
  </Style>

  <Style Target="Paragraph" Name="Code">
    <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
  </Style>

  <Style Target="FormattedText" Name="Code">
    <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
  </Style>

  <Style Target="Cell" Name="Code">
    <Setters Format.Font.Name="Consolas" Format.Font.Size="8" Format.Font.Bold="false" Format.Font.Italic="false"/>
  </Style>

  <Section>
    <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.11 - String Functions</p>

    <p>We've used a few functions in the lessons so far but not really covered them in any kind of detail, the next few lessons will serve as a comprehensive reference of all functions available to you when working with MigraDocXML.</p>

    <Table Borders.Color="Black">
      <Column Width="{Section.PageSetup.ContentWidth * 0.25}" Format.Font.Bold="true"/>
      <Column Width="{Section.PageSetup.ContentWidth * 0.75}"/>

      <Style Target="Row">
        <Setters VerticalAlignment="Center"/>
      </Style>

      <Row C0="Char" C1="Parameters: int utf8Code
                 Returns the char representation of the passed in integer as a string" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{Char(101)}``` >>> {Char(101)}
          ```{Char(241)}``` >>> {Char(241)}
        </C0>
      </Row>

      <Row C0="Contains" C1="Parameters: string input, string searchText
                 Returns true if the input parameter contains the searchText parameter" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{Contains('hello world!', 'llo w')}``` >>> {Contains('hello world!', 'llo w')}
          ```{Contains('hello world!', 'cat')}``` >>> {Contains('hello world!', 'cat')}
        </C0>
      </Row>

      <Row C0="EndsWith" C1="Parameters: string input, string end
                 Returns true if the input parameter ends with the end parameter" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{EndsWith('hello world!', 'ld!')}``` >>> {EndsWith('hello world!', 'ld!')}
          ```{EndsWith('hello world!', 'cat')}``` >>> {EndsWith('hello world!', 'cat')}
        </C0>
      </Row>

      <Row C0="Format" C1="Parameters: object input, string format
                 Applies string formatting to the passed in object, just like sticking a # at the end of an expression" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{Format(2/3.0, '0.000')}``` >>> {Format(2/3.0, '0.000')}</C0>
      </Row>

      <Row C0="IndexOf" C1="Parameters: string text, string find, optional int startIndex
                 Finds the index of the first occurence of the find parameter within the text parameter" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{'abcde'.IndexOf('cd')}``` >>> {'abcde'.IndexOf('cd')}</C0>
      </Row>

      <Row C0="Insert" C1="Parameters: string text, int insertIndex, string insertText
                 Inserts text into an existing string, starting at the specified index" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{'held'.Insert(2, 'llo wor')}``` >>> {'held'.Insert(2, 'llo wor')}</C0>
      </Row>

      <Row C0="IsNullOrEmpty" C1="Returns true if the passed in string is either null or of 0 length" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{IsNullOrEmpty('foot')}``` >>> {IsNullOrEmpty('foot')}
          ```{IsNullOrEmpty('')}``` >>> {IsNullOrEmpty('')}
          ```{IsNullOrEmpty(null)}``` >>> {IsNullOrEmpty(null)}
          ```{IsNullOrEmpty(' ')}``` >>> {IsNullOrEmpty(' ')}
        </C0>
      </Row>

      <Row C0="IsNullOrWhitespace / IsNullOrWhiteSpace" C1="Returns true if the passed in string is either null or contains only white space. You can use either function definition with either lower or upper 'S', I always found it hard to remember which version it should be in C# even with IntelliSense, so both are allowed." KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{IsNullOrWhitespace('foot')}``` >>> {IsNullOrWhitespace('foot')}
          ```{IsNullOrWhiteSpace('')}``` >>> {IsNullOrWhiteSpace('')}
          ```{IsNullOrWhitespace(null)}``` >>> {IsNullOrWhitespace(null)}
          ```{IsNullOrWhiteSpace(' ')}``` >>> {IsNullOrWhiteSpace(' ')}
        </C0>
      </Row>

      <Row C0="LastIndexOf" C1="Parameters: string text, string find, optional int startIndex
                 Finds the index of the last occurence of the find parameter within the text parameter" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{'ababa'.LastIndexOf('b')}``` >>> {'ababa'.LastIndexOf('b')}</C0>
      </Row>

      <Row C0="LeftBrace" C1="Returns the left brace character" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{LeftBrace()}``` >>> {LeftBrace()}</C0>
      </Row>

      <Row C0="NewLine" C1="Returns the new line character" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{'hello' + NewLine() + 'world'}``` >>> {'hello' + NewLine() + 'world'}</C0>
      </Row>

      <Row C0="Pad" C1="Parameters: string input, int requiredLength, string fillChar
                 Pads out a string from the left &amp; right using the fillChar to bring the input up to the required length" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{Pad('Hello', 11, '+')}``` >>> {Pad('Hello', 11, '+')}</C0>
        <C0 Style="Code" MergeRight="1">```{Pad('Hello', 10, '+')}``` >>> {Pad('Hello', 10, '+')}</C0>
        <C0 Style="Code" MergeRight="1">```{Pad('Test', 10, 'x')}``` >>> {Pad('Test', 10, 'x')}</C0>
      </Row>

      <Row C0="PadLeft" C1="Parameters: string input, int requiredLength, string fillChar
                 Pads out a string from the left using the fillChar to bring the input up to the required length" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{PadLeft('7', 3, '0')}``` >>> {PadLeft('7', 3, '0')}</C0>
      </Row>

      <Row C0="PadRight" C1="Parameters: string input, int requiredLength, string fillChar
                 Pads out a string from the right using the fillChar to bring the input up to the required length" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{PadRight('Hi', 5, '-')}``` >>> {PadRight('Hi', 5, '-')}</C0>
      </Row>

      <Row C0="Remove" C1="Parameters: string text, int startIndex, optional int count
                 Removes text from a string, starting at the specified index" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{'beckoned'.Remove(2, 5)}``` >>> {'beckoned'.Remove(2, 5)}</C0>
      </Row>

      <Row C0="Replace" C1="Parameters: string input, string findText, string replaceText
                 Finds all occurences of findText in input and returns a new string replacing them with replaceText" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{Replace('leet', 'e', '3')}``` >>> {Replace('leet', 'e', 3)}
        </C0>
      </Row>

      <Row C0="RightBrace" C1="Returns the right brace character" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{RightBrace()}``` >>> {RightBrace()}</C0>
      </Row>

      <Row C0="Space" C1="Parameters: optional int count
                 Returns one or more non-breaking spaces" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{'Hello' + Space() + 'world'}``` >>> {'Hello' + Space() + 'world'}
          ```{'Hello' + Space(5) + 'world'}``` >>> {'Hello' + Space(5) + 'world'}
        </C0>
      </Row>

      <Row C0="Split" C1="Parameters: string text, string splitter1, optional string splitter2, optional string splitter3, ...
                 Splits the text into an array of strings based on occurences of the splitters" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{'hello world'.Split(' ')}``` >>> ['{'hello world'.Split(' ').MergeToString('\', \'')}']</C0>
      </Row>

      <Row C0="StartsWith" C1="Parameters: string input, string start
                 Returns true if the input parameter starts with the start parameter" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{StartsWith('hello world!', 'hell')}``` >>> {StartsWith('hello world!', 'hell')}
          ```{StartsWith('hello world!', 'dog')}``` >>> {StartsWith('hello world!', 'dog')}
        </C0>
      </Row>

      <Row C0="Substring" C1="Parameters: string input, int startIndex, optional int length
                 Returns only part of the input string, starting from the start index and optionally only up to the specified length" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">
          ```{Substring('hello world!', 2)}``` >>> {Substring('hello world!', 2)}
          ```{Substring('hello world!', 2, 6)}``` >>> {Substring('hello world!', 2, 6)}
        </C0>
      </Row>

      <Row C0="ToLower" C1="Takes a string as input, returns a new copy that's all lowercase" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{ToLower('tEsT')}``` >>> {ToLower('tEsT')}</C0>
      </Row>

      <Row C0="ToTitle" C1="Takes a string as input, returns a new copy where the start of each word is uppercase and the rest lower" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{ToTitle('tEsT')}``` >>> {ToTitle('tEsT')}</C0>
      </Row>

      <Row C0="ToUpper" C1="Takes a string as input, returns a new copy that's all uppercase" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{ToUpper('tEsT')}``` >>> {ToUpper('tEsT')}</C0>
      </Row>

      <Row C0="Trim" C1="Takes a string as input, returns a new copy as output with all whitespace removed from the start and end" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{'h' + Trim(' ell ') + 'o'}``` >>> {'h' + Trim(' ell ') + 'o'}</C0>
      </Row>

      <Row C0="TruncateStart" C1="Parameters: string input, int length
                 If the input string is longer than the length parameter, then the starting characters are removed to bring it down to length" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{TruncateStart('Hello', 4)}``` >>> {TruncateStart('Hello', 4)}</C0>
      </Row>

      <Row C0="TruncateEnd" C1="Parameters: string input, int length
                 If the input string is longer than the length parameter, then the ending characters are removed to bring it down to length" KeepWith="1"/>
      <Row>
        <C0 Style="Code" MergeRight="1">```{TruncateEnd('Hello', 4)}``` >>> {TruncateEnd('Hello', 4)}</C0>
      </Row>
    </Table>

  </Section>
</Document>