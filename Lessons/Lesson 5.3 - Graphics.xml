<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 5.3 - Graphics</p>

        <p>MigraDocXML is built upon MigraDoc, which in turn is built upon a lower-level PDF library called PDFSharp. With PDFSharp you have far more control over drawing, though it comes with the downside that there's more work needed to get things done.</p>

        <p>MigraDocXML supports mixing of MigraDoc &amp; PDFSharp, allowing you to draw graphics on top of a document design.</p>

        <p>For example:</p>

        <p Style="Code">&lt;Graphics Page=&quot;1&quot;&gt;
            {Space(4)}&lt;Rect Left=&quot;2.3cm&quot; Top=&quot;7cm&quot; Width=&quot;16.4cm&quot; Height=&quot;1.6cm&quot; Pen.Color=&quot;Red&quot; Pen.Width=&quot;2&quot; Corner=&quot;3mm&quot;/&gt;
            &lt;/Graphics&gt;
        </p>
        
        <Graphics Page="1">
            <Rect Left="2.3cm" Top="7cm" Width="16.4cm" Height="1.6cm" Pen.Color="Red" Pen.Width="2" Corner="3mm"/>
        </Graphics>

        <p>This example obviously has the downside that we're having to statically place the shape on the page, so is very sensitive to change and generally just not very useful.</p>

        <p>The Graphics tag essentially forms a closure around all variables which are in scope, since the Graphics code is only run after the rest of the document has already been rendered. This allows us access to get the positions of existing elements, meaning you can place graphics in relation to them. There are 4 new scripting functions available for use purely within the <i>Graphics</i> tag to help get the positions of elements:</p>

        <p Format.SpaceBefore="4mm"><b>GetArea</b> gets the area of the passed in DOM element:</p>

        <p Style="Code">&lt;p&gt;Ignore this statement. If already reading this sentence, please stop reading it immediately.
            {Space(4)}&lt;Graphics&gt;
            {Space(8)}&lt;Var pArea=&quot;p.GetArea()&quot;/&gt;
            {Space(8)}&lt;Line Page=&quot;```{pArea.Page}```&quot; Pen.Color=&quot;Red&quot;&gt;
            {Space(12)}&lt;Point X=&quot;```{pArea.Left}&quot; Y=&quot;{pArea.Top}```&quot;/&gt;
            {Space(12)}&lt;Point X=&quot;```{pArea.Right}&quot; Y=&quot;{pArea.Bottom}```&quot;/&gt;
            {Space(8)}&lt;/Line>
            {Space(8)}&lt;Line Page=&quot;```{pArea.Page}```&quot; Pen.Color=&quot;Red&quot;&gt;
            {Space(12)}&lt;Point X=&quot;```{pArea.Left}&quot; Y=&quot;{pArea.Bottom}```&quot;/&gt;
            {Space(12)}&lt;Point X=&quot;```{pArea.Right}&quot; Y=&quot;{pArea.Top}```&quot;/&gt;
            {Space(8)}&lt;/Line&gt;
            {Space(4)}&lt;/Graphics&gt;
            &lt;/p&gt;
        </p>

        <p>Ignore this statement. If already reading this sentence, please stop reading it immediately.
            <Graphics>
                <Var pArea="p.GetArea()"/>
                <Line Page="{pArea.Page}" Pen.Color="Red">
                    <Point X="{pArea.Left}" Y="{pArea.Top}"/>
                    <Point X="{pArea.Right}" Y="{pArea.Bottom}"/>
                </Line>
                <Line Page="{pArea.Page}" Pen.Color="Red">
                    <Point X="{pArea.Left}" Y="{pArea.Bottom}"/>
                    <Point X="{pArea.Right}" Y="{pArea.Top}"/>
                </Line>
            </Graphics>
        </p>

        <p Format.SpaceBefore="4mm"><b>GetAreas</b> gets the areas of the passed in DOM element. For example, if you have a table that spans multiple pages, this will return one area for each page the table exists on:</p>

        <p Style="Code" Format.SpaceAfter="2mm">&lt;Table&gt;
            {Space(4)}&lt;Column Width=&quot;2cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;3cm&quot;/&gt;
            {Space(4)}&lt;Graphics&gt;
            {Space(8)}&lt;ForEach Var=&quot;area&quot; In=&quot;Table.GetAreas()&quot;&gt;
            {Space(12)}&lt;Rect Area=&quot;```{area}```&quot; Pen.Color=&quot;Blue&quot; Corner=&quot;2mm&quot;/&gt;
            {Space(8)}&lt;/ForEach&gt;
            {Space(4)}&lt;/Graphics&gt;

            {Space(4)}&lt;ForEach Var=&quot;i&quot; In=&quot;[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]&quot;&gt;
            {Space(8)}&lt;Row C0=&quot;```{i}&quot; C1=&quot;{i * 3}```&quot;/&gt;
            {Space(4)}&lt;/ForEach&gt;
            &lt;/Table&gt;
        </p>

        <Table>
            <Column Width="2cm"/>
            <Column Width="3cm"/>
            <Graphics>
                <ForEach Var="area" In="Table.GetAreas()">
                    <Rect Area="{area}" Pen.Color="Blue" Corner="2mm"/>
                </ForEach>
            </Graphics>

            <ForEach Var="i" In="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]">
                <Row C0="{i}" C1="{i * 3}"/>
            </ForEach>
        </Table>

        <p Format.SpaceBefore="4mm"><b>GetTaggedAreas</b> gets the nested elements of the passed in DOM element which have their <i>Tag</i> property equal to that of the passed in tag parameter:</p>

        <p Style="Code">&lt;p Tag="test tag"&gt;Hello&lt;/p&gt;
            &lt;p Tag="test tug"&gt;Hello&lt;/p&gt;
            &lt;p&gt;Hello&lt;/p&gt;
            &lt;p Tag="test tag"&gt;Hello&lt;/p&gt;
            &lt;Graphics&gt;
            {Space(4)}&lt;ForEach Var="area" In="Section.GetTaggedAreas('test tag')"&gt;
            {Space(8)}&lt;Bezier Page="```{area.Page}```"&gt;
            {Space(12)}&lt;Point X="```{area.Left}" Y="{area.Bottom}```"/&gt;
            {Space(12)}&lt;Point X="```{area.Left.Points - 10}" Y="{area.Bottom}```"/&gt;
            {Space(12)}&lt;Point X="```{area.Left.Points - 10}" Y="{area.Top}```"/&gt;
            {Space(12)}&lt;Point X="```{area.Left}" Y="{area.Top}```"/&gt;
            {Space(8)}&lt;/Bezier&gt;
            {Space(4)}&lt;/ForEach&gt;
            &lt;/Graphics&gt;
        </p>

        <p Tag="test tag">Hello</p>
        <p Tag="test tug">Hello</p>
        <p>Hello</p>
        <p Tag="test tag">Hello</p>
        <Graphics>
            <ForEach Var="area" In="Section.GetTaggedAreas('test tag')">
                <Bezier Page="{area.Page}">
                    <Point X="{area.Left}" Y="{area.Bottom}"/>
                    <Point X="{area.Left.Points - 10}" Y="{area.Bottom}"/>
                    <Point X="{area.Left.Points - 10}" Y="{area.Top}"/>
                    <Point X="{area.Left}" Y="{area.Top}"/>
                </Bezier>
            </ForEach>
        </Graphics>

        <p Format.SpaceBefore="4mm"><b>GetAreasOfType</b> gets the areas of all DOM elements of a specific type which are children of the passed in DOM element:</p>

        <p Style="Code" Format.SpaceAfter="2mm">&lt;Table&gt;
            {Space(4)}&lt;Column Width=&quot;2cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;3cm&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;4cm&quot;/&gt;
            {Space(4)}&lt;Graphics&gt;
            {Space(8)}&lt;ForEach Var=&quot;area&quot; In=&quot;Table.GetAreasOfType('Column')&quot;&gt;
            {Space(12)}&lt;Rect Area=&quot;```{area}```&quot; Corner=&quot;2mm&quot; Pen.Color=&quot;Purple&quot;/&gt;
            {Space(8)}&lt;/ForEach&gt;
            {Space(4)}&lt;/Graphics&gt;

            {Space(4)}&lt;Row C0=&quot;test&quot; C1=&quot;test&quot; C2=&quot;test&quot;/&gt;
            {Space(4)}&lt;Row C0=&quot;test&quot; C1=&quot;test&quot; C2=&quot;test&quot;/&gt;
            &lt;/Table&gt;
        </p>

        <Table>
            <Column Width="2cm"/>
            <Column Width="3cm"/>
            <Column Width="4cm"/>
            <Graphics>
                <ForEach Var="area" In="Table.GetAreasOfType('Column')">
                    <Rect Area="{area}" Corner="2mm" Pen.Color="Purple"/>
                </ForEach>
            </Graphics>

            <Row C0="test" C1="test" C2="test"/>
            <Row C0="test" C1="test" C2="test"/>
        </Table>

        <p Format.SpaceBefore="4mm"><b>GetPageCount</b> gets the number of pages in the document. The following code uses this to manually add page numbering to each page of the document, instead of the usual approach of using a PageField inside a Header.</p>

        <p Style="Code" Format.SpaceAfter="2mm">&lt;Graphics&gt;
            {Space(4)}&lt;Var pageCount=&quot;Document.GetPageCount()&quot;/&gt;
            {Space(4)}&lt;ForEach Var=&quot;page&quot; In=&quot;Range(1, pageCount)&quot;&gt;
            {Space(8)}```&lt;String Page=&quot;{page}&quot; Point.X=&quot;{Section.PageSetup.PageWidth.Points - 100}&quot; Point.Y=&quot;40&quot; Font.FamilyName=&quot;Arial&quot;&gt;Page {page} of {pageCount}&lt;/String&gt;```
            {Space(4)}&lt;/ForEach&gt;
            &lt;/Graphics&gt;
        </p>

        <Graphics>
            <Var pageCount="Document.GetPageCount()"/>
            <ForEach Var="page" In="Range(1, pageCount)">
                <String Page="{page}" Point.X="{Section.PageSetup.PageWidth.Points - 100}" Point.Y="40" Font.FamilyName="Arial">Page {page} of {pageCount}</String>
            </ForEach>
        </Graphics>
    </Section>
</Document>