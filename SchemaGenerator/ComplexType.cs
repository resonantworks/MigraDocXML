﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public class ComplexType : SchemaType
    {

        public List<AttributeBase> Attributes { get; private set; } = new List<AttributeBase>();

        public List<ElementBase> Elements { get; private set; } = new List<ElementBase>();

        public ComplexExtension Extension { get; set; }

        public bool Mixed { get; set; }

        public ComplexType(string name, bool mixed = false)
            : base(name)
        {
            Mixed = mixed;
        }

        public ComplexType Add(AttributeBase attribute)
        {
            Attributes.Add(attribute);
            return this;
        }

        public ComplexType Add(IEnumerable<AttributeBase> attributes)
        {
            Attributes.AddRange(attributes);
            return this;
        }

        public ComplexType Add(ElementBase element)
        {
            Elements.Add(element);
            return this;
        }

        public ComplexType Add(IEnumerable<ElementBase> elements)
        {
            Elements.AddRange(elements);
            return this;
        }

        public ComplexType ExtendsComplex(string baseType)
        {
            Extension = new ComplexExtension(baseType, true, false);
            return this;
        }

        public ComplexType ExtendsSimple(string baseType)
        {
            Extension = new ComplexExtension(baseType, false, false);
            return this;
        }

        public ComplexType RestrictsComplex(string baseType)
        {
            Extension = new ComplexExtension(baseType, true, true);
            return this;
        }

        public ComplexType RestrictsSimple(string baseType)
        {
            Extension = new ComplexExtension(baseType, false, true);
            return this;
        }


        public class ComplexExtension
        {
            public string BaseType { get; private set; }

            public bool IsRestriction { get; private set; }

            public bool ComplexContent { get; private set; }

            public ComplexExtension(string baseType, bool complexContent = false, bool isRestriction = false)
            {
                BaseType = baseType;
                ComplexContent = complexContent;
                IsRestriction = isRestriction;
            }
        }

    }
}
