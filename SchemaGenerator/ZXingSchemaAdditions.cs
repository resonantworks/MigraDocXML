﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public class ZXingSchemaAdditions
    {

        public void RunBeforeBuild(MigraDocXMLSchema schema)
        {
            schema.ImageElements.Add(new Element("Barcode", "barcodeType"));
            schema.ImageElements.Add(new Element("AztecCode", "aztecCodeType"));
            schema.ImageElements.Add(new Element("Code128", "code128Type"));
            schema.ImageElements.Add(new Element("DataMatrix", "dataMatrixType"));
            schema.ImageElements.Add(new Element("PDF417", "pdf417Type"));
            schema.ImageElements.Add(new Element("QRCode", "qrCodeType"));
        }


        public void RunAfterBuild(MigraDocXMLSchema schema)
        {
            schema.Types.Add(new SimpleType("barcodeFormatType")
                .RestrictByEnumeration("xs:string", "AZTEC", "CODABAR",
                    "CODE_128", "CODE_39", "CODE_93",
                    "DATA_MATRIX", "EAN_13", "EAN_8",
                    "IMB", "ITF", "MAXICODE",
                    "MSI", "PDF_417", "PLESSEY",
                    "QR_CODE", "RSS_14", "RSS_EXPANDED",
                    "UPC_A", "UPC_E", "UPC_EAN_EXTENSION"));

            schema.Types.Add(new SimpleType("dataMatrixShapeType")
                .RestrictByEnumeration("xs:string", "FORCE_NONE", "FORCE_RECTANGLE", "FORCE_SQUARE"));

            schema.Types.Add(new SimpleType("pdf417ErrorCorrectionType")
                .RestrictByEnumeration("xs:string", "AUTO", "L0", "L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8"));

            schema.Types.Add(new SimpleType("qrCodeErrorCorrectionType")
                .RestrictByEnumeration("xs:string", "L", "M", "Q", "H"));

			schema.Types.Add(new SimpleType("barcodeRotationType")
				.RestrictByEnumeration("xs:int", "0", "90", "180", "270"));

			schema.Types.Add(new ComplexType("barcodeType", mixed: true)
                .ExtendsComplex("shapeType")
                .Add(new Attribute("LockAspectRatio", "boolType"))
                .Add(new Attribute("Resolution", "doubleType"))
                .Add(new Attribute("ScaleHeight", "doubleType"))
                .Add(new Attribute("ScaleWidth", "doubleType"))

                .Add(new Attribute("Format", "barcodeFormatType"))
                .Add(new Attribute("GS1Format", "boolType"))
                .Add(new Attribute("PixelHeight", "intType"))
                .Add(new Attribute("BarcodeMargin", "intType"))
                .Add(new Attribute("PixelWidth", "intType"))
                .Add(new Attribute("PureBarcode", "boolType"))
				.Add(new Attribute("Rotation", "barcodeRotationType"))
				.Add(new Attribute("FlipX", "boolType"))
				.Add(new Attribute("FlipY", "boolType")));

            schema.Types.Add(new ComplexType("aztecCodeType")
                .ExtendsComplex("barcodeType")
                .Add(new Attribute("ErrorCorrection", "intType"))
                .Add(new Attribute("Layers", "intType")));

            schema.Types.Add(new ComplexType("code128Type")
                .ExtendsComplex("barcodeType")
                .Add(new Attribute("ForceCodesetB", "boolType")));

            schema.Types.Add(new ComplexType("dataMatrixType")
                .ExtendsComplex("barcodeType")
                .Add(new Attribute("DefaultEncodation", "intType"))
                .Add(new Attribute("MaxWidth", "intType"))
                .Add(new Attribute("MaxHeight", "intType"))
                .Add(new Attribute("MinWidth", "intType"))
                .Add(new Attribute("MinHeight", "intType"))
                .Add(new Attribute("SymbolShape", "dataMatrixShapeType")));

            schema.Types.Add(new ComplexType("pdf417Type")
                .ExtendsComplex("barcodeType")
                .Add(new Attribute("AspectRatio", "xs:string"))
                .Add(new Attribute("CharacterSet", "xs:string"))
                .Add(new Attribute("Compact", "boolType"))
                .Add(new Attribute("Compaction", ""))
                .Add(new Attribute("MinCols", "intType"))
                .Add(new Attribute("MaxCols", "intType"))
                .Add(new Attribute("MinRows", "intType"))
                .Add(new Attribute("MaxRows", "intType"))
                .Add(new Attribute("DisableECI", "boolType"))
                .Add(new Attribute("ErrorCorrection", "pdf417ErrorCorrectionType")));

            schema.Types.Add(new ComplexType("qrCodeType")
                .ExtendsComplex("barcodeType")
                .Add(new Attribute("CharacterSet", "xs:string"))
                .Add(new Attribute("DisableECI", "boolType"))
                .Add(new Attribute("ErrorCorrection", "qrCodeErrorCorrectionType"))
                .Add(new Attribute("QrVersion", "intType")));
        }

    }
}
