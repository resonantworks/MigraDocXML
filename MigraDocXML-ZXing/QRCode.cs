﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocXML_ZXing
{
    public class QRCode : Barcode
    {

        private ZXing.QrCode.QrCodeEncodingOptions _options;


        public QRCode()
            : base()
        {
            _options = new ZXing.QrCode.QrCodeEncodingOptions();
            _writer.Options = _options;
            _writer.Format = ZXing.BarcodeFormat.QR_CODE;
		}


		public override void SetTextValue(string value)
		{
			if (GS1Format)
			{
				List<KeyValuePair<string, string>> splits = null;
				//If we can't even succeed in splitting out the text into a GS1 compliant format, then treat it as not GS1
				try
				{
					splits = GS1.SplitReadableText(value);
				}
				catch
				{
					base.SetTextValue(value);
					return;
				}

				string gs1Text = GS1.BuildGS1Barcode(splits, (char)29);
				base.SetTextValue(gs1Text);
			}
			else
				base.SetTextValue(value);
		}


		public string CharacterSet { get => _options.CharacterSet; set => _options.CharacterSet = value; }

        public bool DisableECI { get => _options.DisableECI; set => _options.DisableECI = value; }

        public string ErrorCorrection
        {
            get => _options.ErrorCorrection.ToString();
            set
            {
                switch (value)
                {
                    case "L":
                        _options.ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.L;
                        break;
                    case "M":
                        _options.ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.M;
                        break;
                    case "Q":
                        _options.ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.Q;
                        break;
                    case "H":
                        _options.ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.H;
                        break;
                }
            }
        }

        public int? QrVersion { get => _options.QrVersion; set => _options.QrVersion = value; }

    }
}
