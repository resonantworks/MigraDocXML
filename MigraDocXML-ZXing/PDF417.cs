﻿using MigraDocXML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocXML_ZXing
{
    public class PDF417 : Barcode
    {

        private ZXing.PDF417.PDF417EncodingOptions _options;


        public PDF417()
            : base()
        {
            _options = new ZXing.PDF417.PDF417EncodingOptions();
            _writer.Options = _options;
            _writer.Format = ZXing.BarcodeFormat.PDF_417;
        }


        public string AspectRatio
        {
            get => _options.AspectRatio.ToString();
            set => _options.AspectRatio = (ZXing.PDF417.Internal.PDF417AspectRatio)Enum.Parse(typeof(ZXing.PDF417.Internal.PDF417AspectRatio), value);
        }

        public string CharacterSet { get => _options.CharacterSet; set => _options.CharacterSet = value; }

        public bool Compact { get => _options.Compact; set => _options.Compact = value; }

        public string Compaction
        {
            get => _options.Compaction.ToString();
            set => _options.Compaction = Parse.Enum<ZXing.PDF417.Internal.Compaction>(value);
        }

        public int MinCols
        {
            get => _options.Dimensions?.MinCols ?? -1;
            set => _options.Dimensions = new ZXing.PDF417.Internal.Dimensions(value, _options.Dimensions?.MaxCols ?? value, _options.Dimensions?.MinRows ?? value, _options.Dimensions?.MaxRows ?? value);
        }

        public int MaxCols
        {
            get => _options.Dimensions?.MaxCols ?? -1;
            set => _options.Dimensions = new ZXing.PDF417.Internal.Dimensions(_options.Dimensions?.MinCols ?? value, value, _options.Dimensions?.MinRows ?? value, _options.Dimensions?.MaxRows ?? value);
        }

        public int MinRows
        {
            get => _options.Dimensions?.MinRows ?? -1;
            set => _options.Dimensions = new ZXing.PDF417.Internal.Dimensions(_options.Dimensions?.MinCols ?? value, _options.Dimensions?.MaxCols ?? value, value, _options.Dimensions?.MaxRows ?? value);
        }

        public int MaxRows
        {
            get => _options.Dimensions?.MaxRows ?? -1;
            set => _options.Dimensions = new ZXing.PDF417.Internal.Dimensions(_options.Dimensions?.MinCols ?? value, _options.Dimensions?.MaxCols ?? value, _options.Dimensions?.MinRows ?? value, value);
        }

        public bool DisableECI { get => _options.DisableECI; set => _options.DisableECI = value; }

        public string ErrorCorrection
        {
            get => _options.ErrorCorrection.ToString();
            set => _options.ErrorCorrection = Parse.Enum<ZXing.PDF417.Internal.PDF417ErrorCorrectionLevel>(value);
        }

    }
}
