﻿using MigraDocXML;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Drawing;

namespace TestConsole
{
	/// <summary>
	/// This is just used as a scratchpad
	/// </summary>
	class Program
	{

		static void Main(string[] args)
		{
			try
			{
				Lesson3_7_Code.Run();
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);
				if(ex.InnerException != null)
				{
					Console.WriteLine();
					Console.WriteLine("Inner Exception:");
					Console.WriteLine(ex.InnerException.Message);
					Console.WriteLine(ex.InnerException.StackTrace);
				}
				Console.ReadKey();
			}
		}
	}
}
