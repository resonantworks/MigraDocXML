﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class BookmarkTests
    {
        [Fact]
        public void SimpleBookmark()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p>This is a test<Bookmark>Bookmark Name</Bookmark></p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            var pElements = p.GetParagraphModel().Elements;
            Assert.Equal(2, pElements.Count);
            Assert.Equal("This is a test", pElements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("Bookmark Name", pElements.OfType<MigraDoc.DocumentObjectModel.Fields.BookmarkField>().First().Name);
        }
    }
}
