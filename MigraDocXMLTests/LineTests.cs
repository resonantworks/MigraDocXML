﻿using MigraDocXML;
using MigraDocXML.DOM;
using MigraDocXML.DOM.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
	public class LineTests
	{
		[Fact]
		public void PageInheritedFromGraphics()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Graphics Page=\"3\">" +
							"<Line/>" +
						"</Graphics>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Graphics gfx = section.Children.OfType<Graphics>().First();
			gfx.ChildProcessor();
			Line line = gfx.Children.OfType<Line>().First();

			Assert.Equal(3, gfx.Page);
			Assert.Equal(3, line.Page);
		}

		[Fact]
		public void GraphicsPageOverwritten()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Graphics Page=\"3\">" +
							"<Line Page=\"7\"/>" +
						"</Graphics>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Graphics gfx = section.Children.OfType<Graphics>().First();
			gfx.ChildProcessor();
			Line line = gfx.Children.OfType<Line>().First();

			Assert.Equal(3, gfx.Page);
			Assert.Equal(7, line.Page);
		}

		[Fact]
		public void PointsAdded()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<Graphics Page=\"3\">" +
							"<Line>" +
								"<Point X=\"1cm\" Y=\"2cm\"/>" +
								"<Point X=\"3cm\" Y=\"4cm\"/>" +
							"</Line>" +
						"</Graphics>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Graphics gfx = section.Children.OfType<Graphics>().First();
			gfx.ChildProcessor();
			Line line = gfx.Children.OfType<Line>().First();
			var points = line.Children.OfType<Point>().ToList();

			Assert.Equal(2, points.Count);
			Assert.Equal(line, points[0].GetGraphicalParent());
			Assert.Equal(line, points[1].GetGraphicalParent());
		}
	}
}
