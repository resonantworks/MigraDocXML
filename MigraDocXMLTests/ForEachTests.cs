﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class ForEachTests
    {
        [Fact]
        public void SimpleForEach()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<ForEach Var=\"item\" In=\"Model\">" +
                            "<p>{item}</p>" +
                        "</ForEach>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run(new[] { 2, 3, 5 });
            Section section = doc.Children.OfType<Section>().First();

            Assert.Single(section.Children);
            Assert.IsType<ForEach>(section.Children.First());
            Assert.Equal(3, section.GetSectionModel().Elements.Count);

            var paragraphs = section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().ToList();
            Assert.Equal("2", paragraphs[0].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("3", paragraphs[1].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("5", paragraphs[2].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }

        [Fact]
        public void LambdaForEach()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<ForEach Var=\"item\" In=\"Model.Where(x => x >= 3)\">" +
                            "<p>{item}</p>" +
                        "</ForEach>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run(new[] { 2, 3, 5 });
            Section section = doc.Children.OfType<Section>().First();

            Assert.Single(section.Children);
            Assert.IsType<ForEach>(section.Children.First());
            Assert.Equal(2, section.GetSectionModel().Elements.Count);

            var paragraphs = section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().ToList();
            Assert.Equal("3", paragraphs[0].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("5", paragraphs[1].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }
    }
}
