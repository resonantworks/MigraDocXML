﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class WhileTests
    {
        [Fact]
        public void SimpleWhile()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Var count=\"0\"/>" +
                        "<While Test=\"count < 4\">" +
                            "<p>{count}</p>" +
                            "<Set count=\"count + 1\"/>" +
                        "</While>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Equal(2, section.Children.Count());
            Assert.IsType<Var>(section.Children.First());
            Assert.IsType<While>(section.Children.Last());
            Assert.Equal(4, section.GetSectionModel().Elements.Count);

            var paragraphs = section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().ToList();
            Assert.Equal("0", paragraphs[0].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("1", paragraphs[1].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("2", paragraphs[2].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("3", paragraphs[3].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }
    }
}
