﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class TextTests
    {
        [Fact]
        public void TextTest()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p><Text>This is a test</Text></p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            var pElements = p.GetParagraphModel().Elements;
            Assert.Single(pElements);
            Assert.Equal("This is a test", pElements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }
    }
}
