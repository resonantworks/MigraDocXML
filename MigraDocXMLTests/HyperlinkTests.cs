﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class HyperlinkTests
    {
        [Fact]
        public void HyperlinkTest()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p><a>This is a link</a></p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            var pElements = p.GetParagraphModel().Elements;
            Assert.Single(pElements);
            Assert.Equal("This is a link", pElements
                .OfType<MigraDoc.DocumentObjectModel.Hyperlink>().First()
                .Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First()
                .Content
            );
        }
    }
}
