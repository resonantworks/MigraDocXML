﻿using MigraDocXML;
using MigraDocXML.DOM;
using MigraDocXML.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class SetTests
    {
        [Fact]
        public void SetUndefinedVariable()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Set test=\"1\"/>" +
                    "</Section>" +
                "</Document>";

            Assert.Throws<UndefinedVariableException>(() => new PdfXmlReader() { DesignText = code }.Run());
        }

        [Fact]
        public void SetVariable()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Var test=\"0\"/>" +
                        "<Set test=\"1\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Assert.Equal(1, section.GetVariable("test"));
        }

        [Fact]
        public void SetMultipleVariables()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Var test1=\"0\" test2=\"5\"/>" +
                        "<Set test1=\"test1 + 1\" test2=\"test2 + 1\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Assert.Equal(1, section.GetVariable("test1"));
            Assert.Equal(6, section.GetVariable("test2"));
        }
    }
}
